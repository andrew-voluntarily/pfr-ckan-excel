Attribute VB_Name = "CustomRibbonButtons"
Option Explicit
Private Const msMODULE As String = "CustomRibbonButtons"

Sub Custom_Btn_CleanSheet(control As IRibbonControl)
  Call ClearSheet
End Sub
Sub Custom_Btn_CleanSearch(control As IRibbonControl)
  Call ClearSearch
End Sub
Sub Custom_Btn_GetProjectCode(control As IRibbonControl)
  Call GetProjectCode
End Sub

Sub Custom_Btn_UpdateSheetIndex(control As IRibbonControl)
  Call UpdateSheetIndex
End Sub
Sub Custom_Btn_UpdateDataHub(control As IRibbonControl)
  Call UpdateDataHub
End Sub
Sub Custom_Btn_MergeFromWorkbook(control As IRibbonControl)
  Call MergeFromWorkbook
End Sub



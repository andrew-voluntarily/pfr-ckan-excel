# Plant and Food Research - CKAN Metadata Catalogue
# Services for Excel Spreadsheets.

This project contains VBA scripts for Excel that allow the lookup of project codes from the CKAN API and populating metadata pages in an Excel file. 

It includes VBA Macro files that provide a WebClient to support the calling of the CKAN API and filling in metadata page details

TODO:
Add script that pushes the spreadsheet into CKAN.

Setup data in dev environment
Somehow lock down the dataset resource ID so that it is predictable - e.g by alias?




